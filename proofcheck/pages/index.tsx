import { useState } from "react";
import type { NextPage } from "next";
import Head from "next/head";
import Dropzone from "react-dropzone";
import {
  Box,
  Container,
  Grid,
  Typography,
  Button,
  CircularProgress,
} from "@mui/material";
import { ArrowDownwardOutlined as ArrowDownIcon } from "@mui/icons-material";
import embed from "vega-embed";
// @ts-ignore
import install from "../python/install.py";
// @ts-ignore
import proof from "../python/proof.py";

let pyodide: any = null;

const proofcheckContext = {
  proofData: "",
};

const Home: NextPage = () => {
  const [fileName, setFileName] = useState("");
  const [showProgress, setShowProgress] = useState(false);

  const checkProofData = async () => {
    setShowProgress(true);

    if (!pyodide) {
      // @ts-ignore
      pyodide = await loadPyodide();
      await pyodide.loadPackage("micropip");
      await pyodide.runPythonAsync(install);
      pyodide.registerJsModule("proofcheck", proofcheckContext);
    }

    const spec = await pyodide.runPythonAsync(proof, proofcheckContext);
    await embed("#vis", JSON.parse(spec));

    setShowProgress(false);
  };

  const fileReceived = (files: File[]) => {
    if (files.length > 0) {
      const file = files[0];
      setFileName(file.name);
      const reader = new window.FileReader();
      reader.onload = (event: any) => {
        const csv = event.target.result as string;
        proofcheckContext.proofData = csv;
        console.log(csv);
        checkProofData();
      };
      reader.readAsText(file);
    }
  };

  return (
    <>
      <Head>
        <title>Proofcheck for Proofmode</title>
      </Head>

      <Container maxWidth="md" sx={{ pt: 10 }}>
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography
              variant="h3"
              sx={{ fontWeight: 700, textAlign: "center" }}
            >
              <span style={{ color: "darkseagreen" }}>Proof</span>
              <span style={{ color: "darkgreen" }}>check</span>
              <span style={{ color: "grey", fontSize: "70%", margin: 5 }}>
                {" "}
                for{" "}
              </span>
              <span style={{ color: "darkseagreen" }}>Proof</span>
              <span style={{ color: "darkgreen" }}>mode</span>
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1" sx={{ textAlign: "left" }}>
              Proofcheck verifies your Proofmode data in your browser. No proof
              data is sent to our servers in the process.
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1" sx={{ textAlign: "left" }}>
              To get started, drag/drop or select your Proofmode .csv data
              below. The file will verified and a data integrity report will
              appear.
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2" sx={{ color: "darkgreen", mt: 2 }}>
              Hint: Open your browser console to see the Python output
            </Typography>
          </Grid>
          <Grid item>
            <Box
              sx={{
                maxWidth: 600,
                margin: "0 auto",
                p: 6,
                mt: 6,
                border: "1px solid #aaa",
              }}
            >
              <Dropzone
                onDrop={(acceptedFiles: any) => fileReceived(acceptedFiles)}
              >
                {({ getRootProps, getInputProps }: any) => (
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <Grid
                      container
                      direction="column"
                      alignItems="center"
                      alignContent="center"
                    >
                      <Grid item>
                        <Typography
                          variant="body1"
                          style={{ textAlign: "center", paddingTop: 10 }}
                        >
                          Drag/drop your file here
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography variant="caption" style={{ color: "#aaa" }}>
                          or
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Button
                          variant="contained"
                          sx={{ backroundColor: "green", color: "white" }}
                        >
                          Select
                        </Button>
                      </Grid>
                      <Grid>
                        <Typography variant="h6">{fileName}</Typography>
                      </Grid>
                    </Grid>
                  </div>
                )}
              </Dropzone>
            </Box>
          </Grid>
          <Grid item sx={{ textAlign: "center" }}>
            {showProgress ? (
              <CircularProgress />
            ) : (
              <ArrowDownIcon
                style={{
                  width: 80,
                  height: 80,
                  margin: "0px auto",
                }}
              />
            )}
          </Grid>
          <Grid item>
            <Box
              sx={{
                border: "1px solid #aaa",
                minWidth: 600,
                minHeight: 400,
                m: "0 auto",
                mt: 2,
                mb: 5,
                textAlign: "center",
                p: 2,
              }}
            >
              <Box
                id="vis"
                sx={{
                  m: "0 auto",
                }}
              />
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Home;
