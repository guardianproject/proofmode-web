import "../styles/globals.css";
import Script from "next/script";
import type { AppProps } from "next/app";

function Proofcheck({ Component, pageProps }: AppProps) {
  return (
    <>
      <Script
        async
        src="https://cdn.jsdelivr.net/pyodide/v0.20.0/full/pyodide.js"
      />
      <Component {...pageProps} />
    </>
  );
}

export default Proofcheck;
